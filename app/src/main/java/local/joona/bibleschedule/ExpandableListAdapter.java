package local.joona.bibleschedule;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private Map<String, List<String>> childList;
    private String[] bookNames;
    private OnItemClick callBack;

    public ExpandableListAdapter(Context context, Map<String, List<String>> childList, String[] bookNames, OnItemClick callBack) {
        this.context = context;
        this.childList = childList;
        this.bookNames = bookNames;
        this.callBack = callBack;
    }

    @Override
    public int getGroupCount() {
        return this.bookNames.length;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.bookNames[groupPosition];
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.childList.get(this.bookNames[groupPosition]).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_group, null);
        }
        TextView headerTextView = convertView.findViewById(R.id.listHeader_textView);
        TextView readChaptersTextView = convertView.findViewById(R.id.read_chapters_textView);
        headerTextView.setText(bookNames[groupPosition]);
        readChaptersTextView.setText(String.valueOf(Collections.frequency(childList.get(bookNames[groupPosition]), "1")) + "/" + String.valueOf(childList.get(bookNames[groupPosition]).size()));
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String headerText = (String) getGroup(groupPosition);
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, null);
        }
        List<String> chapters;
        chapters = childList.get(headerText);
        MyGridView gridView = convertView.findViewById(R.id.grid_view);
        GridViewAdapter adapter = new GridViewAdapter(chapters, context, groupPosition + 1, callBack, bookNames[groupPosition]);
        gridView.setAdapter(adapter);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
