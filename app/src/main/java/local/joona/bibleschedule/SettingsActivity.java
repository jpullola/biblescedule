package local.joona.bibleschedule;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import java.util.Calendar;

public class SettingsActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, SimpleDialog.SimpleDialogListener {

    LinearLayout dailyGoal_linearLayout, reminderTime_linearLayout;
    TextView dailyGoalValue_textView, reminderTime_hour_textView, reminderTime_minute_textView;
    DatabaseHelper databaseHelper;
    CheckBox showReminders_checkBox;
    int dailyGoal, showReminders, reminderHour, reminderMinute;
    AlertDialog alertDialog;
    EditText editText;
    SharedPreferences.Editor editor;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.settings);
        setSupportActionBar(toolbar);
        databaseHelper = new DatabaseHelper(this);


        //Get settings from preferences
        preferences = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        dailyGoal = preferences.getInt(getString(R.string.daily_goal_preference_key), 1);
        showReminders = preferences.getInt(getString(R.string.show_reminders_preference_key), 0);
        reminderHour = preferences.getInt(getString(R.string.reminder_hour_preference_key), 20);
        reminderMinute = preferences.getInt(getString(R.string.reminder_minute_preference_key), 0);


        //Initialize layout variables
        dailyGoal_linearLayout = findViewById(R.id.daily_goal_linearLayout);
        reminderTime_linearLayout = findViewById(R.id.reminders_time_linearLayout);
        dailyGoalValue_textView = findViewById(R.id.daily_goal_value);
        reminderTime_hour_textView = findViewById(R.id.reminders_time_hour);
        reminderTime_minute_textView = findViewById(R.id.reminders_time_minute);
        showReminders_checkBox = findViewById(R.id.reminders_checkBox);


        //Set settings values for layouts
        dailyGoalValue_textView.setText(String.valueOf(dailyGoal));
        reminderTime_hour_textView.setText(String.format("%02d", reminderHour));
        reminderTime_minute_textView.setText(String.format("%02d", reminderMinute));

        if(showReminders == 1){
            showReminders_checkBox.setChecked(true);
            setReminderTimeClickable(true);
        }
        else{
            showReminders_checkBox.setChecked(false);
            setReminderTimeClickable(false);
        }


        //Initialize dialog for getting user input for daily goal
        editText = new EditText(this);
        LinearLayout dialogLayout = new LinearLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER;

        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText.setLayoutParams(params);

        dialogLayout.addView(editText);
        dialogLayout.setPadding(60, 20, 60, 0);

        alertDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.daily_goal)
                .setView(dialogLayout)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editor = preferences.edit();
                        dailyGoal = Integer.parseInt(editText.getText().toString());
                        editor.putInt(getString(R.string.daily_goal_preference_key), dailyGoal);
                        editor.apply();
                        dailyGoalValue_textView.setText(String.valueOf(dailyGoal));
                        dialog.dismiss();
                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();


        //Show dialog when daily goal setting is clicked
        dailyGoal_linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.show();
            }
        });


        //Listener for checkbox
        showReminders_checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor = preferences.edit();
                if(isChecked) {
                    editor.putInt(getString(R.string.show_reminders_preference_key), 1);
                    editor.apply();
                    setReminderTimeClickable(true);
                    setReminder(reminderHour, reminderMinute);
                } else {
                    editor.putInt(getString(R.string.show_reminders_preference_key), 0);
                    editor.apply();
                    setReminderTimeClickable(false);
                    cancelReminder();
                }

                showReminders = preferences.getInt(getString(R.string.show_reminders_preference_key), 1);
            }
        });


        //Show timepicker when reminder time setting is clicked
        reminderTime_linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(), "time picker");
            }
        });

    }

    private void setReminderTimeClickable (boolean clickable){
        reminderTime_linearLayout.setClickable(clickable);
        if(clickable) {
            reminderTime_linearLayout.setAlpha((float) 1);
        } else {
            reminderTime_linearLayout.setAlpha((float) 0.38);
        }
    }

    private void setReminder(int hour, int minute) {
        editor = preferences.edit();
        editor.putInt(getString(R.string.reminder_hour_preference_key), hour);
        editor.putInt(getString(R.string.reminder_minute_preference_key), minute);
        editor.apply();

        Calendar currentTime = Calendar.getInstance();
        int currentDayOfMonth = currentTime.get(Calendar.DAY_OF_MONTH);
        int currentMonth = currentTime.get(Calendar.MONTH);
        int currentYear = currentTime.get(Calendar.YEAR);

        Calendar setTime= Calendar.getInstance();
        setTime.set(Calendar.HOUR_OF_DAY, hour);
        setTime.set(Calendar.MINUTE, minute);
        setTime.set(Calendar.SECOND, 0);

        //Check if the alarm should be fired already today
        if (currentTime.getTimeInMillis() < setTime.getTimeInMillis()){
            setTime.set(Calendar.DAY_OF_MONTH, currentDayOfMonth);
            setTime.set(Calendar.MONTH, currentMonth);
            setTime.set(Calendar.YEAR, currentYear);
        } else {
            if (currentDayOfMonth == currentTime.getActualMaximum(Calendar.DAY_OF_MONTH)) {
                setTime.set(Calendar.DAY_OF_MONTH, 1);
                if (currentMonth == 11){
                    setTime.set(Calendar.MONTH, 0);
                    setTime.set(Calendar.YEAR, (currentYear + 1));
                } else {
                    setTime.set(Calendar.MONTH, (currentMonth + 1));
                    setTime.set(Calendar.YEAR, currentYear);
                }
            } else {
                setTime.set(Calendar.DAY_OF_MONTH, (currentDayOfMonth + 1));
                setTime.set(Calendar.MONTH, currentMonth);
                setTime.set(Calendar.YEAR, currentYear);
            }
        }

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, NotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, setTime.getTimeInMillis(), pendingIntent);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, setTime.getTimeInMillis(), pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, setTime.getTimeInMillis(), pendingIntent);
        }
    }

    private void cancelReminder() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, NotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);
    }


    //Set reminder time text
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        reminderTime_hour_textView.setText(String.format("%02d", hourOfDay));
        reminderTime_minute_textView.setText(String.format("%02d", minute));
        setReminder(hourOfDay, minute);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem settingsItem = menu.findItem(R.id.action_settings);
        settingsItem.setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_clear_all:
                openDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openDialog(){
        SimpleDialog simpleDialog = new SimpleDialog();

        Bundle bundle = new Bundle();
        bundle.putString("Dialog_Message", getResources().getString(R.string.clear_all_confirm));
        bundle.putString("Dialog_Positive_Option", getResources().getString(R.string.clear));
        bundle.putString("Dialog_Negative_Option", getResources().getString(R.string.cancel));

        simpleDialog.setArguments(bundle);
        simpleDialog.show(getSupportFragmentManager(), "Clear");
    }

    @Override
    public void passResult(boolean result) {
        if(result){
            Calendar calendar = Calendar.getInstance();
            databaseHelper.clearAllReadChapters();
            editor = preferences.edit();
            editor.putInt(getString(R.string.recently_cleared_all_preference_key), 1);
            editor.putInt(getString(R.string.daily_read_state_preference_key), 0);

            //Set next day for the readState to be cleared
            if (calendar.get(Calendar.DAY_OF_MONTH) == calendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
                editor.putInt(getString(R.string.next_read_state_clear_preference_key), 1);
            } else {
                editor.putInt(getString(R.string.next_read_state_clear_preference_key), (calendar.get(Calendar.DAY_OF_MONTH) + 1));
            }

            editor.apply();
        }
    }
}
