package local.joona.bibleschedule;

public interface OnItemClick {
    void updateTotalReadChapters(boolean increase);
}
