package local.joona.bibleschedule;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import java.util.Calendar;

public class NotificationReceiver extends BroadcastReceiver {

    NotificationManager notificationManager;
    int hour, minute, readState, dailyGoal, dayOfMonth, nextClear, month, year;
    Intent newIntent;


    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Calendar calendar = Calendar.getInstance();
        newIntent = new Intent(context.getApplicationContext(), MainActivity.class);

        //Get information needed for the alarm
        readState = preferences.getInt(context.getString(R.string.daily_read_state_preference_key), 0);
        dailyGoal = preferences.getInt(context.getString(R.string.daily_goal_preference_key), 1);
        nextClear = preferences.getInt(context.getString(R.string.next_read_state_clear_preference_key), 0);
        hour = preferences.getInt(context.getString(R.string.reminder_hour_preference_key), 20);
        minute = preferences.getInt(context.getString(R.string.reminder_minute_preference_key), 0);

        //Set up new day, month and year for the next notification
        if (calendar.get(Calendar.DAY_OF_MONTH) == calendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
            dayOfMonth = 1;
            if (calendar.get(Calendar.MONTH) == 11) {
                month = 0;
                year = (calendar.get(Calendar.YEAR) + 1);
            } else {
                month = (calendar.get(Calendar.MONTH) + 1);
                year = calendar.get(Calendar.YEAR);
            }
        } else {
            dayOfMonth = (calendar.get(Calendar.DAY_OF_MONTH) + 1);
            month = calendar.get(Calendar.MONTH);
            year = calendar.get(Calendar.YEAR);
        }

        //Clear daily read state if not cleared today
        if (calendar.get(Calendar.DAY_OF_MONTH) >= nextClear) {
            editor.putInt(context.getString(R.string.daily_read_state_preference_key), 0);
            readState = 0;

            //Set next day for the readState to be cleared
            if (nextClear == calendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
                nextClear = 1;
            } else {
                nextClear += 1;
            }
            editor.putInt(context.getString(R.string.next_read_state_clear_preference_key), nextClear);
            editor.apply();
        }

        if (readState < dailyGoal){
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, newIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle()
                    .setBigContentTitle(context.getString(R.string.remember_read))
                    .bigText(context.getString(R.string.daily_goal_not_reached));

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context.getApplicationContext(), "notify001")
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle(context.getString(R.string.remember_read))
                    .setContentText(context.getString(R.string.daily_goal_not_reached))
                    .setPriority(Notification.PRIORITY_MAX)
                    .setStyle(bigTextStyle)
                    .setSound(sound)
                    .setAutoCancel(true);

            notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                String channelId = "notify001";
                NotificationChannel channel = new NotificationChannel(channelId, context.getString(R.string.notification_channel_ID), NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(channel);
                builder.setChannelId(channelId);
            }
                notificationManager.notify(0, builder.build());
        }

        rescheduleAlarm(context, intent);

    }

    private void rescheduleAlarm(Context context, Intent intent){
        Calendar newCalendar = Calendar.getInstance();
        newCalendar.set(Calendar.HOUR_OF_DAY, hour);
        newCalendar.set(Calendar.MINUTE, minute);
        newCalendar.set(Calendar.SECOND, 0);
        newCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        newCalendar.set(Calendar.MONTH, month);
        newCalendar.set(Calendar.YEAR, year);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, newCalendar.getTimeInMillis(), pendingIntent);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, newCalendar.getTimeInMillis(), pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, newCalendar.getTimeInMillis(), pendingIntent);
        }
    }
}
