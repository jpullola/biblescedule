package local.joona.bibleschedule;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;
import java.util.List;

public class GridViewAdapter extends BaseAdapter {

    public List<String> chapters;
    Context context;
    DatabaseHelper databaseHelper;
    int bookNumber;
    private OnItemClick callBack;
    AlertDialog alertDialog;
    String bookName;


    public GridViewAdapter(List<String> chapters, Context context, int bookNumber, OnItemClick callBack, String bookName) {
        this.chapters = chapters;
        this.context = context;
        this.bookNumber = bookNumber;
        this.callBack = callBack;
        this.bookName = bookName;
    }

    @Override
    public int getCount() {
        return chapters.size();
    }

    @Override
    public Object getItem(int position) {
        return chapters.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        final TextView gridItem;
        if (convertView == null){
            gridItem = new TextView(context);
            gridItem.setLayoutParams(new GridView.LayoutParams(dpToPx(50),dpToPx(50)));
            gridItem.setPadding(4,4,4,4);
            gridItem.setText(String.valueOf(position + 1));
            gridItem.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            gridItem.setTextColor(context.getResources().getColor(android.R.color.white));
            gridItem.setGravity(Gravity.CENTER);

            if(chapters.get(position).equals("0"))
                gridItem.setBackgroundColor(context.getResources().getColor(R.color.unread_color));
            else
                gridItem.setBackgroundColor(context.getResources().getColor(R.color.read_color));

            gridItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (chapters.get(position).equals("0")){
                        databaseHelper = new DatabaseHelper(context);
                        databaseHelper.changeChapterReadState(bookNumber, position + 1, false);
                        gridItem.setBackgroundColor(context.getResources().getColor(R.color.read_color));
                        chapters.set(position, "1");
                        callBack.updateTotalReadChapters(true);
                    } else {
                        databaseHelper = new DatabaseHelper(context);
                        databaseHelper.changeChapterReadState(bookNumber, position + 1, true);
                        gridItem.setBackgroundColor(context.getResources().getColor(R.color.unread_color));
                        chapters.set(position, "0");
                        callBack.updateTotalReadChapters(false);
                    }
                }
            });

            gridItem.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    final String url = context.getResources().getString(R.string.bible_url) + String.valueOf(bookNumber) + "/" + String.valueOf(position + 1);
                    String dialogTitle = context.getResources().getString(R.string.open_question) + bookName + " " + context.getResources().getString(R.string.chapter) + " " + String.valueOf(position + 1) + context.getResources().getString(R.string.in_browser);
                    LayoutInflater dialogInflater = LayoutInflater.from(context);
                    final View dialogView = dialogInflater.inflate(R.layout.browser_dialog_layout, null);
                    TextView dialogTextView = dialogView.findViewById(R.id.browserDialog_textView);
                    dialogTextView.setText(url);
                    alertDialog = new AlertDialog.Builder(context)
                            .setTitle(dialogTitle)
                            .setView(dialogView)
                            .setPositiveButton(R.string.open, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                    context.startActivity(intent);
                                    dialog.dismiss();
                                }
                            }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create();
                    alertDialog.show();
                    return true;
                }
            });

        } else {
            gridItem = (TextView) convertView;
        }
        return gridItem;
    }

    public int dpToPx(int dp) {
        float density = context.getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }
}
