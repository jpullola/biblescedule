package local.joona.bibleschedule;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "BibleBooks.db";
    private static String DB_PATH = "";
    private static final int DB_VERSION = 1;

    private final Context mContext;

    protected DatabaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
        DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        this.mContext = context;

        copyDatabase();

        this.getReadableDatabase();
    }

    private boolean checkDatabase(){
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    private void copyDatabase(){
        if(!checkDatabase()){
            this.getReadableDatabase();
            this.close();
            try{
                copyDBFile();
            } catch (IOException e){
                throw new Error("ErrorCopyingDataBase");
            }
        }
    }

    private void copyDBFile() throws IOException{
        InputStream inputStream = mContext.getAssets().open(DB_NAME);
        OutputStream outputStream = new FileOutputStream(DB_PATH + DB_NAME);
        byte[] buffer = new byte[1024];
        int length;
        while((length = inputStream.read(buffer)) > 0)
            outputStream.write(buffer, 0, length);
        outputStream.flush();
        outputStream.close();
        inputStream.close();
    }

    protected List<String> getReadChapters(int bookNumber) {
        SQLiteDatabase db = getReadableDatabase();
        List<String> readChapters = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT read FROM [book" + String.valueOf(bookNumber) + "]", null);
        while(cursor.moveToNext()){
            readChapters.add(cursor.getString(cursor.getColumnIndexOrThrow("read")));
        }
        cursor.close();
        db.close();
        return readChapters;
    }

    protected int getTotalReadChapters() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT read FROM books where _id = ?", new String[] { "67" });
        cursor.moveToFirst();
        int totalReadChapters = cursor.getInt(cursor.getColumnIndexOrThrow("read"));
        cursor.close();
        db.close();
        return totalReadChapters;
    }

    protected void changeChapterReadState(int bookNumber, int chapter, boolean currentState) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT read FROM books WHERE _id = ? OR _id = ?", new String[] { String.valueOf(bookNumber), "67" });
        cursor.moveToFirst();
        int currentNumOfReadChapters = cursor.getInt(cursor.getColumnIndexOrThrow("read"));
        cursor.moveToNext();
        int currentNumOfTotalReadChapters = cursor.getInt(cursor.getColumnIndexOrThrow("read"));
        cursor.close();
        if (currentState) {
            db.execSQL("UPDATE [book" + String.valueOf(bookNumber) + "] SET read = " + 0 + " WHERE _id = ?", new String[]{String.valueOf(chapter)});
            db.execSQL("UPDATE books SET read = " + (currentNumOfReadChapters - 1) + " WHERE _id = ?", new String[] { String.valueOf(bookNumber) });
            db.execSQL("UPDATE books SET read = " + (currentNumOfTotalReadChapters - 1) + " WHERE _id = ?", new String[] { "67" });
        } else {
            db.execSQL("UPDATE [book" + String.valueOf(bookNumber) + "] SET read = " + 1 + " WHERE _id = ?", new String[]{String.valueOf(chapter)});
            db.execSQL("UPDATE books SET read = " + (currentNumOfReadChapters + 1) + " WHERE _id = ?", new String[] { String.valueOf(bookNumber) });
            db.execSQL("UPDATE books SET read = " + (currentNumOfTotalReadChapters + 1) + " WHERE _id = ?", new String[] { "67" });
        }
        db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    protected void clearAllReadChapters() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT read FROM books", null);
        cursor.moveToFirst();
        for (int i = 1; i <= 66; i++) {
            if (cursor.getInt(cursor.getColumnIndexOrThrow("read")) > 0){
                db.execSQL("UPDATE book" + i + " SET read = 0");
            }
            db.execSQL("UPDATE books SET read = 0");
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
    }
}
