package local.joona.bibleschedule;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnItemClick {

    DatabaseHelper databaseHelper;
    ExpandableListAdapter listAdapter;
    ExpandableListView expandableListView;
    HashMap<String, List<String>> childList;
    String[] bookNames;
    public List<String> readChapters = new ArrayList<>();
    TextView total_readState_textView, today_readState_textView, total_percentage_textView;
    int readState, totalReadChapters;
    double readPercentage;
    SharedPreferences.Editor editor;
    SharedPreferences preferences;
    View readingProgressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(toolbar);

        readingProgressView = getLayoutInflater().inflate(R.layout.reading_progress_layout, null);
        total_readState_textView = readingProgressView.findViewById(R.id.total_read_state_value_textView);
        today_readState_textView = readingProgressView.findViewById(R.id.today_read_state_value_textView);
        total_percentage_textView = readingProgressView.findViewById(R.id.total_read_state_percentage_textView);

        databaseHelper = new DatabaseHelper(this);
        preferences = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        expandableListView = findViewById(R.id.list_expand);
        prepareListData();
        readPercentage = 100.0 * ((double) totalReadChapters) / 1189.0;
        String percentage = String.format("%.1f", readPercentage) + "%";
        total_readState_textView.setText(String.valueOf(totalReadChapters));
        total_percentage_textView.setText(String.valueOf(percentage));
        today_readState_textView.setText(String.valueOf(readState));

        listAdapter = new ExpandableListAdapter(this, childList, bookNames, this);
        expandableListView.addHeaderView(readingProgressView);
        expandableListView.setAdapter(listAdapter);
    }

    private void prepareListData(){
        childList = new HashMap<>();
        bookNames = getResources().getStringArray(R.array.book_names);
        Calendar calendar = Calendar.getInstance();

        for (int i = 0; i < 66; i++) {
            readChapters = databaseHelper.getReadChapters(i + 1);
            childList.put(bookNames[i], readChapters);
        }
        totalReadChapters = databaseHelper.getTotalReadChapters();

        //Clear daily read state if not cleared today
        if (calendar.get(Calendar.DAY_OF_MONTH) >= preferences.getInt(getString(R.string.next_read_state_clear_preference_key), 0)) {
            editor = preferences.edit();
            editor.putInt(getString(R.string.daily_read_state_preference_key), 0);

            //Set next day for the readState to be cleared
            if (calendar.get(Calendar.DAY_OF_MONTH) == calendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
                editor.putInt(getString(R.string.next_read_state_clear_preference_key), 1);
            } else {
                editor.putInt(getString(R.string.next_read_state_clear_preference_key), (calendar.get(Calendar.DAY_OF_MONTH) + 1));
            }
            editor.apply();
        }

        readState = preferences.getInt(getString(R.string.daily_read_state_preference_key), 0);
    }

    @Override
    public void updateTotalReadChapters(boolean increase) {
        editor = preferences.edit();

        listAdapter.notifyDataSetChanged();
        if (increase) {
            readState += 1;
            totalReadChapters += 1;
        } else if(readState > 0) {
            readState -= 1;
            if (totalReadChapters > 0)
                totalReadChapters -= 1;
        } else if (totalReadChapters > 0) {
            totalReadChapters -= 1;
        }
        editor.putInt(getString(R.string.daily_read_state_preference_key), readState);
        editor.apply();
        readPercentage = 100.0 * ((double) totalReadChapters) / 1189.0;
        String percentage = String.format("%.1f", readPercentage) + "%";
        total_readState_textView.setText(String.valueOf(totalReadChapters));
        total_percentage_textView.setText(String.valueOf(percentage));
        today_readState_textView.setText(String.valueOf(readState));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem clearAllItem = menu.findItem(R.id.action_clear_all);
        clearAllItem.setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        int recentlyClearedAll = preferences.getInt(getString(R.string.recently_cleared_all_preference_key), 0);
        if (recentlyClearedAll == 1){
            prepareListData();
            total_readState_textView.setText("0");
            today_readState_textView.setText("0");
            readState = 0;
            listAdapter = new ExpandableListAdapter(this, childList, bookNames, this);
            expandableListView.setAdapter(listAdapter);

            editor = preferences.edit();
            editor.putInt(getString(R.string.recently_cleared_all_preference_key), 0);
            editor.apply();
        }
    }
}
